"""Simple password manager, with master password. Can be enhanced with GUI, decoupling, etc."""
from cryptography.fernet import Fernet


def create_key():
    crypt_key = Fernet.generate_key()
    with open("crypt_key.key", "wb") as crypt_key_file:
        crypt_key_file.write(crypt_key)


def use_key():
    with open("crypt_key.key", "rb") as crypt_key_file:
        crypt_key = crypt_key_file.read()
        return crypt_key


print("This is a Password Manager.")
master_pswrd = input("Please type in Master Password.\n")
create_key()
master_key = use_key() + master_pswrd.encode()
fernet_use = Fernet(master_key)


def view_pass():
    with open("accounts.txt", "r") as file:
        for line in file.readlines():
            data = line.rstrip()
            account, pswrd = data.split("|", True)
            print("Account name:", account, "Password:", pswrd)
# fernet_use.decrypt(pswrd.encode()).decode()


def add_pass():
    account_name = input("Provide account name: \n")
    account_pswrd = input("Provide account password: \n")
    with open("accounts.txt", "a") as file:
        file.write(account_name + "|" + fernet_use.encrypt(account_pswrd.encode()).decode() + "\n")


while True:
    working_mode = input("Write your choice : add a (N)ew password, (V)iew existing ones, or (Q)uit.\n").lower()
    if working_mode == "q":
        break
    if working_mode == "v":
        view_pass()
    elif working_mode == "n":
        add_pass()
    else:
        print("This is not a valid answer.")
        continue
