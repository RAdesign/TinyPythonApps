"""Simple passwords' generator. Improvements: type validation, retyping mistakes, try blocks."""
import random
import string

password = []

characters_left = 0  # just to init


def update_chars_left(used_characters):
    global characters_left  # to use global, not local variable
    if used_characters < 0 or used_characters > characters_left:
        print(f"Characters input out of allowed value of 0 - {characters_left}")
    else:
        characters_left -= used_characters
        print(f"There are {characters_left} characters left.")


password_len = int(input("How many characters do you wish your password to have (min 5)?\n"))
if password_len < 5:
    print("Password is too short. Minimum 5 characters. Try again.")
else:
    characters_left = password_len

lowercase_amount = int(input("How many lowercase characters do you wish your password to have?\n"))
update_chars_left(lowercase_amount)

uppercase_amount = int(input("How many uppercase characters do you wish your password to have?\n"))
update_chars_left(uppercase_amount)

special_char_amount = int(input("How many special characters do you wish your password to have?\n"))
update_chars_left(special_char_amount)

numbers_amount = int(input("How many number characters do you wish your password to have?\n"))
update_chars_left(numbers_amount)

if characters_left > 0:
    print(f"There are {characters_left} free characters left, they will be chosen form lowercase pool")
    lowercase_amount += characters_left

print(f"Your final password length is {password_len}")
print(f"Lowercase letters:  {lowercase_amount}\n"
      f"Uppercase amount:  {uppercase_amount}\n"
      f"Special characters amount:  {special_char_amount}\n"
      f"Numbers amount:  {numbers_amount}\n")


for item in range(password_len):
    if lowercase_amount > 0:
        password.append(random.choice(string.ascii_lowercase))
        lowercase_amount -= 1
    if uppercase_amount > 0:
        password.append(random.choice(string.ascii_uppercase))
        uppercase_amount -= 1
    if special_char_amount > 0:
        password.append(random.choice(string.punctuation))
        special_char_amount -= 1
    if numbers_amount > 0:
        password.append(random.choice(string.digits))
        numbers_amount -= 1

random.shuffle(password)
print(f"Your finished password is:")
print("".join(password))

# or just use this:
"""password = "".join(random.choice(string.ascii_letters + string.punctuation + string.digits) 
                   for _ in range(10))"""
