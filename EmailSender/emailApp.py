# needs email address and 2FA and password

from email.message import EmailMessage
from EmailSender.app_pasw import password
import ssl
import smtplib

email_sender = 'myownemail@op.pl'
email_password = password

# use temp-mail to generate temporary email
email_receiver = "temporary-email12345@temp-mail.com"

subject = "Call me maybe."
body = """
    Just some email text body over here.
"""

em = EmailMessage()
em["From"] = email_sender
em["To"] = email_receiver
em["Subject"] = subject
em.set_content(body)

context = ssl.create_default_context()
with smtplib.SMTP_SSL("smtp.op.pl", 465, context=context) as smtp:
    smtp.login(email_sender, email_password)
    smtp.sendmail(email_sender, email_receiver, em.as_string())
