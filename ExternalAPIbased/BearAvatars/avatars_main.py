"""Basic use of a free API of avatar bears. www.kaggle.com - many APIs to test things."""
# import random
import requests
from pathlib import Path


response = requests.get('https://api.dicebear.com/6.x/thumbs/svg?seed=Felix&rotate=270&eyes=variant1W12')

Path('./avatars').mkdir(exist_ok=True)

with open('./avatars/avatar.svg', 'wb') as file:
    file.write(response.content)
