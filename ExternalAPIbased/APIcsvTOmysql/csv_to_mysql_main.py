"""Usage of a CSV module, to rewrite data into MySQL. Data form Kaggle.co
DB from freemysqlhosting.net. Improve by adding error handling"""
import csv
import mysql.connector
from contextlib import closing


db_conn_info = {
    "user": "",
    "passwd": "",
    "host": "",
    "port": "",
    "database": ""
}

# query = "SELECT * FROM BTC_TEST"

with closing(mysql.connector.connect(**db_conn_info)) as mysql_conn:
    # with closing(mysql_conn.cursor()) as cur:  # with closing() version
    #   cur.execute(query)
    #   result = cur.fetchall()
    # an alternative version
    cursor = mysql_conn.cursor()
    with open('./data/btc_price_1_week_test.csv') as btc_test_csv:
        reader_dict = csv.DictReader(btc_test_csv)
        for row in reader_dict:
            # remember SQL naming conventions for DB elements
            sql = f"""INSERT INTO
            BTC_TEST(Date, Open, High, Low, Close, Volume, Market_Cap)  
            VALUES('{row['Date']}', {float(row['Open'])}, {float(row['High'])}, {float(row['Low'])}, 
{float(row['Close'])}, {int(float(row['Volume']))}, {int(float(row['Market_Cap']))} )"""
            cursor.execute(sql)
        mysql_conn.commit()


            #sql = f"""INSERT INTO
            #BTC_TEST(Date, Open, High, Low, Close, Volume,Market Cap)
            #VALUES('{row['Date']}', {float(row['Open'])}, {float(row['High'])}, {float(row['Low'])},
            #{float(row['Close'])}, {int(float(row['Volume']))}, {int(float(row['Market Cap']))})"""
            #cursor.execute(sql)


"""with open('./data/btc_price_1_week_test.csv') as btc_test_csv:
    reader_dict = csv.DictReader(btc_test_csv)
    for row in reader_dict:
        print(row['Volume'])

    reader = csv.reader(btc_test_csv)  # normal reader takes Head as well
    for row in reader:
        print(row[5])"""


# alternative :
"""class DBConnection:  # Context managers have to be defined with __enter__ and __exit__ attributes.

    def __init__(self):
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="",
            database="database_name"
        )
        self.cur = self.mydb.cursor()


def __enter__(self):
    return self


def __exit__(self, exc_type, exc_val, exc_tb):
    # close db connection
    self.mydb.connection.close()"""