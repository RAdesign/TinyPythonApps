"""check if where is web connection to the site. To do - various errors reporting"""
import urllib.request
from urllib import request


def check_website(website):

    print(f'checking connection to {website}')

    response = urllib.request.urlopen(website)
    print(f'connection to {website}')
    print(f'connection status: {response.getcode()}')


test_website = input('please enter the website (full name) to check\n')
check_website(test_website)