"""A simple guessing game. Using loops, try-except, while. To be improved with stats, a simple GUI"""
import random

print("Welcome to the random numbers guessing game.")

while True:
    try:
        users_top_numb = int(input("How large should be your guessing pool? \n Write a number here: "))
        if users_top_numb > 5:
            break
        else:
            print("Your value is too low for a fair game. Try something above 5.")
    except ValueError:
        print("That was not a number ! Please try again.")


rand_number = random.randint(0, users_top_numb)
guess_count = 0

while True:
    user_guess = input("What number was chosen? Please make a guess: ")
    if user_guess.isdigit():
        user_guess = int(user_guess)
        if user_guess > users_top_numb:
            print(f"Your input is out of range of {users_top_numb}")
            continue
        else:
            guess_count += 1
    else:
        print("Your input was not a number, try again.")
        continue

    if user_guess is rand_number:
        print(f"Yes, the {rand_number} is the right number !")
        print(f"You guessed it in {guess_count} tries !")
        break
    elif user_guess > rand_number:
        print(f"Your guess is above the number. You have {10 - guess_count} guesses left.")
    else:
        print(f"Your guess is below the number. You have {10 - guess_count} guesses left.")
    if guess_count == 10:
        print("Unfortunately you`ve ran out of guesses.")
        print(f"The number you`ve failed to guess is: {rand_number}")
        break

print("Thank you for playing Guess the Number!")
