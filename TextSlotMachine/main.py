# a simple slot machine game
import random

MAX_LINES = 4  # capitals for constant naming routine
MAX_BET = 1000
MIN_BET = 1

ROWS = 4
COLUMNS = 4

symbol_count = {   # slot symbols count in each reel
    "A": 2,
    "B": 4,
    "C": 6,
    "D": 8,
}

symbol_value = {  # value of each symbol
    "A": 4,
    "B": 3,
    "C": 2,
    "D": 1,
}


def check_win_conditions(columns, lines, bet, values):
    win_rate = 0
    winning_lines = []
    for line in range(lines):
        symbol = columns[0][line]
        for column in columns:
            symbol_check = column[line]
            if symbol != symbol_check:
                break
        else:
            win_rate += values[symbol] * bet
            winning_lines.append(line + 1)
    return win_rate, winning_lines


def spin_slot_machine(rows, cols, symbols):
    all_symbols = []
    for symbol, symbol_count in symbols.items():
        for _ in range(symbol_count):   # _ is used to loop through anonymously without need to count
            all_symbols.append(symbol)

    columns = []
    for _ in range(cols):
        column = []
    # copy symbols to a new list, for removal after choice, : works as slice operator, w/o it`d be a reference
        current_symbols = all_symbols[:]
        for _ in range(rows):
            value = random.choice(all_symbols)
            current_symbols.remove(value)
            column.append(value)

        columns.append(column)
    return columns


def print_slot_machine(columns):   # arranging visual view of columns by transposing matrix
    for row in range(len(columns[0])):  # assuming that there is at least 1 column
        for i, column in enumerate(columns):
            if i != len(columns) - 1:
                print(column[row], end=" | ")
            else:
                print(column[row], end="")

        print("")


def deposit():
    while True:
        amount = input("How much would you like to deposit in $ ? \n")
        if amount.isdigit():
            amount = int(amount)
            if amount > 0:
                break
            else:
                print("Amount must be greater than 0")
    return amount


def get_number_of_lines():
    while True:
        lines = input("Enter number of lines to bet (1-" + str(MAX_LINES) + ") \n")
        if lines.isdigit():
            lines = int(lines)
            if 1 <= lines <= MAX_LINES:
                break
            else:
                print("Enter a valid number of lines.")
        else:
            print("Please enter a number.")
    return lines


def get_bet():
    while True:
        amount = input("How much $ would you like to bet on each line ? \n")
        if amount.isdigit():
            amount = int(amount)
            if MIN_BET <= amount <= MAX_BET:
                break
            else:
                print(f"Amount must be between {MIN_BET} and {MAX_BET}")
        else:
            print("Please enter a number.")
    return amount


def spin(balance):
    lines = get_number_of_lines()
    while True:
        bet = get_bet()
        total_bet = bet * lines

        if total_bet > balance:
            print(f"Your total bet of {total_bet}$ is larger than your balance of {balance}$. \n"
                  f"You need to bet less.")
        else:
            break
    print(f"You are betting {bet}$ on {lines} lines. \n"
          f"Total bet is equal to {total_bet}$")

    slots = spin_slot_machine(ROWS, COLUMNS, symbol_count)
    print_slot_machine(slots)
    win_rate, winning_lines = check_win_conditions(slots, lines, bet, symbol_value)
    print(f"You won {win_rate}$.")
    print(f"You won on lines : ", *winning_lines)  # splat operator unpacks data
    return win_rate - total_bet


def main():
    balance = deposit()
    while True:
        print(f"Current balance is {balance}$")
        decision = input("Press enter to spin another game, or Q to quit game.")
        if decision == "q":
            break
        balance += spin(balance)
        if balance == 0:
            print("Your balance is zero, you can not play anymore.")
            break

    print(f"You left with {balance}$.")


main()
