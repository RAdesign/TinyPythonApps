Some small apps made with Python.
To be updated with cleaner code, extra functionalities.

1. An e-mail sender application
2. Word Replacement app
3. Basic Calculator
4. Text Slot Machine
5. Typical Quiz Game
6. Password Manager
7. Number Guessing
8. Rock Paper Scissors
9. Paragraph Adventure