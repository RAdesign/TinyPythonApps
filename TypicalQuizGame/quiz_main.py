"""Basic Quiz Game. Can be improved by adding RegEx and String Formatting for the answers, so that
users mistakes can be handled"""
print("Welcome, this is a computer quiz !")

will_play = input("Would you like to start a game ?\n Please confirm(y): ")

if will_play not in {"yes", "Yes", "yeah", "y"}:
    print("Goodbye then !")
    quit()

score: int = 0

answer = input("How many coconuts a swallow can carry? \n Answer :").lower()
if answer in {"african?", "european?", "from europe?", "from africa?", "which one?"}:
    score += 1
    print(f"Well, that`s a good question. I don`t know. \nYou win! Your score is: {score}.")
else:
    print(f"Wrong answer! Your score is: {score}.")

answer = input("Where The Holy Hand Grenade comes from? \n Answer :").lower()
if answer in {"antioch", "from antioch", "city of antioch"}:
    score += 1
    print(f"Well, that`s right. \nYou win! Your score is: {score}.")
else:
    if score > 0:
        score -= 1
    print(f"Wrong answer! Your score is: {score}.")

answer = input("If a women weights the same as a duck, or is made of wood, is she a witch? \n Answer :").lower()
if answer in {"yes", "yes!", "yeah", "yeah!"}:
    score += 1
    print(f"That`s right! She`s a witch! \nYou win! Your score is: {score}.")
else:
    if score > 0:
        score -= 1
    print(f"Wrong answer, and you are cursed by a witch! Your score is: {score}.")

print(f"That`s the end of this quiz, your overall score is: {score}")
