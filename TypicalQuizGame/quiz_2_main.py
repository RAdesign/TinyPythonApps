"""Another attempt to a quiz game, this time with external resource. To improve: GUI, random question,
points table, data type validation, second guess, discard used question, question categories, difficulty
levels, various points amount, 'don`t know' answer, negative points, hints."""
import json

points_won = 0


def question_handling(question):
    global points_won
    print()
    print(question["question"])
    print("a:", question["a"])
    print("b:", question["b"])
    print("c:", question["c"])
    print("d:", question["d"])
    print("e:", question["e"])

    answer = input("Which answer do you choose?\n")

    if answer == question["good"]:
        print("It`s a good answer !")
        points_won += 1
    else:
        print("Unfortunately, it`s a wrong answer.")


with open("quiz_2.json") as json_quiz_data:
    questions = json.load(json_quiz_data)

    for item in range(0, len(questions)):
        question_handling(questions[item])


print(f"You have won {points_won} points !")
