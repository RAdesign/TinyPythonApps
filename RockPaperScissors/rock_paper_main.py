"""A simple Rock Paper Scissors game. With loops, try-except, while. Tracking counts.
To be improved with GUI, table score."""

import random


user_score = 0
comp_score = 0
user_symbols = ["r", "p", "s"]
game_symbols = ["rock", "paper", "scissors"]

while True:
    user_input = input("Type : (R)ock / (P)aper / (S)cissors, or type (Q) to quit\n").lower()
    if user_input == "q":
        print("Goodbye and thank you for playing!")
        break
    if user_input not in user_symbols:
        raise ValueError("Please write correct answer type")

    rand_symbol = random.randint(0, 2)
    comp_choice = game_symbols[rand_symbol]
    print(f"Computer picked {comp_choice}.")

    if user_input == "r" and comp_choice == "scissors":
        print("Rock beats scissors ! You win !")
        user_score += 1

    elif user_input == "p" and comp_choice == "rock":
        print("Paper beats rock ! You win !")
        user_score += 1

    elif user_input == "s" and comp_choice == "paper":
        print("Scissors beat paper ! You win !")
        user_score += 1

    elif (user_input == "p" and comp_choice == "paper" or user_input == "s" and comp_choice == "scissors"
          or user_input == "r" and comp_choice == "rock"):
        print("It`s a draw !")

    else:
        print("You loose !")
        comp_score += 1

    print(f"Your score is: {user_score} \nComputer score is: {comp_score}")

print(f"Your final score is: {user_score} \nComputer final score is: {comp_score}")
if user_score > comp_score:
    print("You beat computer !")
elif user_score < comp_score:
    print("You loose to a mere machine...")
else:
    print("It`s a fair draw.")
