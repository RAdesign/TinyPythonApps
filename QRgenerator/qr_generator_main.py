"""Simple app to convert any text into a QR code, with simple library"""
import qrcode


def generate_qr(qr_text):
    qr = qrcode.QRCode(
        version=1,
        error_correction= qrcode.constants.ERROR_CORRECT_L,
        box_size=12,
        border=4,
    )

    qr.add_data(qr_text)
    qr.make(fit=True)
    qr_image = qr.make_image(fill_color=(0, 0, 0), back_color=(255, 255, 255))
    qr_image.save('it_s_a_trap.png')


test_text = 'https://jquery.com'

generate_qr(test_text)
