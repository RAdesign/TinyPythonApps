"""Using match case or dict in a simple b-day naming app."""
import calendar
import datetime


def translate_match_pl(day_name):
    match day_name:
        case "Monday":
            return "Poniedziałek"
        case "Tuesday":
            return "Wtorek"
        case "Wednesday":
            return "Środa"
        case "Thursday":
            return "Czwartek"
        case "Friday":
            return "Piątek"
        case "Saturday":
            return "Sobota"
        case "Sunday":
            return "Niedziela"


def translate_dict_pl(day_name):
    eng_pl_day = {
        "Monday": "Poniedziałek",
        "Tuesday": "Wtorek",
        "Wednesday": "Środa",
        "Thursday": "Czwartek",
        "Friday": "Piątek",
        "Saturday": "Sobota",
        "Sunday": "Niedziela"
    }
    return eng_pl_day[day_name]


birth_date = input("Please input your date of birth [dd-mm-yyyy] format.\n")
day, month, year = birth_date.split("-")

birth_date = datetime.datetime(int(year), int(month), int(day))

name_of_day = calendar.day_name[birth_date.weekday()]
print(translate_match_pl(name_of_day))
