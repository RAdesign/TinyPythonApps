"""automated messaging of SMS using textbelt.com"""
import schedule
import requests
import pytz
from personal_data import personal_number


def sending_sms():
    resp = requests.post('https://textbelt.com/text', {
        'phone': personal_number,
        'message': 'jaki jest ten format e.164 dla polski',
        'key': 'textbelt',
    })
    print(resp.json())


# schedule.every().day.at('21:37:58', 'Europe/Warsaw').do(sending_sms)

sending_sms()