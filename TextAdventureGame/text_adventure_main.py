"""Simple paragraph game, with a lot of room to expand and improve, like GUI, tables, more choices"""
import random

player_health: int = 10
player_coins: int = 0
player_items = []

print("Welcome to the Forest maze game.")
player_name = input("What is your name, stranger ?\n").capitalize()
if player_name != "":
    print(f"Hello, {player_name} !")
else:
    print("So, you decided to remain a stranger. I shall call you likewise.")
    player_name = "Stranger"
    print(f"Hello, {player_name} !")

while True:
    print(f"You wake up in an empty clearing, within a forest.\n"
          f"You assess yourself. You have {player_health} health, and {player_coins} coins in your pocket."
          f"You have {len(player_items)} items.")
    if len(player_items) != 0:
        print(f"You have {player_items} with you.")
    answer = input("You can always stop this game. Just type (q)uit. Or else, to continue..").lower()
    if answer == "q":
        want_quit = input("Do you really want to quit? Is so - type (y)es.").lower()
        if want_quit == "y":
            print(f"I never thought, you`re a quitter, {player_name}.")
            break
    else:
        print(f"So, {player_name}, You`ve decided to play along. You need 10 coins to win.\n"
              f"...or 0 health, to loose.")
    answer1 = input("Try your luck, and start (s)earching your surroundings, or move (f)orward. ").lower()
    if answer1 == "s":
        search1 = random.randint(0, 3)
        if search1 == 0:
            print("You are out of luck this time, You have found nothing.")
            print(f"You still have {player_coins} coins")
        else:
            print(f"You were lucky. You have found {search1} coins!")
            player_coins += search1
            print(f"You now have {player_coins} coins")
            if player_coins >= 10:
                break
    elif answer1 == "f":
        damage1 = random.randint(0, 2)
        if damage1 == 0:
            print("You go forward, to another clearing.")
        else:
            print(f"You were very unlucky. You stumbled upon some brambles, and suffered {damage1} damage.")
            player_health -= damage1
            print(f"You now have {player_health} health")
            if player_coins <= 0:
                break

# to be continued
