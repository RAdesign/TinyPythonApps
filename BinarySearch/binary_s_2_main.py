"""Works for sorted lists, O-notation for binary -> O(log(n)), linear search -> O(n)"""


# linear search
import random
import time


def linear_search(s_list, s_target):
    for i in range(len(s_list)):
        if s_list[i] == s_target:
            return i
    return -1  # for no effect


# binary search for sorted list
def binary_search(s_list, s_target, low=None, high=None):
    if low is None:
        low = 0
    if high is None:
        high = len(s_list) - 1
    if high < low:
        return -1

    midpoint = (low + high) // 2
    # check if midpoint == s_target, if not, whether it`s on left or right side
    if s_list[midpoint] == s_target:
        return midpoint
    elif s_target < s_list[midpoint]:
        return binary_search(s_list, s_target, low, midpoint-1)
    else:
        return binary_search(s_list, s_target, midpoint + 1, high)


if __name__ == '__main__':
    # test_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    # test_target = 7
    # print(linear_search(test_list, test_target))
    # print(binary_search(test_list, test_target))

    length = 10000
    sorted_list = set()
    while len(sorted_list) < length:
        sorted_list.add(random.randint(-3*length, 3*length))
    sorted_list = sorted(list(sorted_list))

    start = time.time()
    for target in sorted_list:
        binary_search(sorted_list, target)
    end = time.time()
    print(f'Binary search time: {end-start} seconds')
    for target in sorted_list:
        linear_search(sorted_list, target)
    end = time.time()
    print(f'Linear search time: {end - start} seconds')
