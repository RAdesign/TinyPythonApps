"""binary search through a list, w/ target parameter, no recursion.
shows number of steps of binary search"""


def binary_search(b_list, b_element):
    start = 0
    end = len(b_list)
    steps = 1  # start index for better visual representation

    while start <= end:
        print(f"Step {steps}, : {b_list[start:end+1]}")

        steps += 1
        middle = (start+end) // 2

        if b_element == b_list[middle]:
            return middle
        elif b_element < b_list[middle]:
            end = middle - 1
        else:
            start = middle + 1
    return -1


test_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
test_number = 7

print(binary_search(test_list, test_number))