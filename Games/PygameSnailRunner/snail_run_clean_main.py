import pygame
from sys import exit
from random import randint

pygame.init()

WIDTH = 800
HEIGHT = 460

GAME_WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('Snail Runner')

clock = pygame.time.Clock()
test_font = pygame.font.Font('font/Pixeltype.ttf', 50)


def display_timer():
    current_timer = int(pygame.time.get_ticks()/1500) - start_time
    surface_score = test_font.render(f"Score:{current_timer}", False, (64, 64, 64))
    rect_score = surface_score.get_rect(center=(WIDTH/2, 50))
    GAME_WINDOW.blit(surface_score, rect_score)
    return current_timer


def enemy_movement(enemy_list):
    if enemy_list:
        for enemy_rect in enemy_list:
            if enemy_rect.bottom == surface_sky.get_height():
                enemy_rect.x -= 6
                GAME_WINDOW.blit(snail_surf, enemy_rect)
            else:
                enemy_rect.x -= 3
                GAME_WINDOW.blit(fly_surf, enemy_rect)
        enemy_list = [enemy for enemy in enemy_list if enemy.x > -100]
        return enemy_list
    else:
        return []


def collisions(player, enemies):
    if enemies:
        for enemy_rect in enemies:
            if player.colliderect(enemy_rect):
                return False

    return True


def player_animation():  # for animating player img - walking on floor, jump in air
    global player_surf, player_index  # 2 globals to use out of scope

    if rect_player.bottom < surface_sky.get_height():  # jump
        player_surf = player_jump
    else:  # walk
        player_index += 0.1  # as it starts with 0, float increment simulates slow change
        if player_index >= len(player_walk):
            player_index = 0
        player_surf = player_walk[int(player_index)]


# basic assets
surface_sky = pygame.image.load('graphics/sky.png').convert()
surface_ground = pygame.image.load('graphics/ground.png').convert()

# enemies - animated
snail_frame_1 = pygame.image.load('graphics/snail/snail1.png').convert_alpha()
snail_frame_2 = pygame.image.load('graphics/snail/snail2.png').convert_alpha()
snail_frames = [snail_frame_1, snail_frame_2]
snail_frame_index = 0
snail_surf = snail_frames[snail_frame_index]
fly_frame_1 = pygame.image.load('graphics/fly/Fly1.png').convert_alpha()
fly_frame_2 = pygame.image.load('graphics/fly/Fly2.png').convert_alpha()
fly_frames = [fly_frame_1, fly_frame_2]
fly_frame_index = 0
fly_surf = fly_frames[fly_frame_index]

enemy_rect_list = []

# player - animated
player_walk_1 = pygame.image.load('graphics/player/player_walk_1.png').convert_alpha()
player_walk_2 = pygame.image.load('graphics/player/player_walk_2.png').convert_alpha()
player_walk = [player_walk_1, player_walk_2]
player_index = 0  # for choice of animation frame
player_jump = pygame.image.load('graphics/player/jump.png').convert_alpha()

player_surf = player_walk[player_index]
rect_player = player_surf.get_rect(midbottom=(80, surface_sky.get_height()))
gravity_fall_speed = 0

# title screen elements
player_stand = pygame.image.load('graphics/player/player_stand.png').convert_alpha()
player_stand = pygame.transform.rotozoom(player_stand, 0.0, 2.0)
rect_player_stand = player_stand.get_rect(center=(WIDTH/2, HEIGHT/2))
game_title = test_font.render('...Snail...Runner...', False, (50, 80, 110))
rect_title = game_title.get_rect(midtop=(WIDTH/2, game_title.get_height()))
game_hint = test_font.render('Press   SPACE   to   Jump   or   to   Continue', False, (50, 80, 110))
rect_hint = game_hint.get_rect(midbottom=(WIDTH/2, HEIGHT-game_title.get_height()))

game_active = True
start_time = 0
title_score = 0

# timers - for animation
timer_obstacle = pygame.USEREVENT+1
pygame.time.set_timer(timer_obstacle, 900)
snail_animation_timer = pygame.USEREVENT+2  # new timers for animating each enemy
pygame.time.set_timer(snail_animation_timer, 100)
fly_animation_timer = pygame.USEREVENT+3
pygame.time.set_timer(fly_animation_timer, 100)


while True:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()

        if game_active:
            display_timer()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if rect_player.collidepoint(event.pos) and rect_player.bottom == surface_sky.get_height():
                    gravity_fall_speed = - 22

            if event.type == pygame.KEYDOWN:  # checks if any key gets pressed
                if event.key == pygame.K_SPACE and rect_player.bottom == surface_sky.get_height():
                    gravity_fall_speed = -22

        else:
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                game_active = True
                start_time = int(pygame.time.get_ticks()/1700)

        if game_active:  # simplification of active loop
            if event.type == timer_obstacle:
                if randint(0, 2):
                    enemy_rect_list.append(snail_surf.get_rect(bottomleft=(randint(900, 2800),
                                                                           surface_sky.get_height())))
                else:
                    enemy_rect_list.append(fly_surf.get_rect(bottomleft=(600, surface_sky.get_height() / 2)))

            if event.type == snail_animation_timer:  # event loops for each enemy animation
                if snail_frame_index == 0:
                    snail_frame_index = 1
                else:
                    snail_frame_index = 0
                snail_surf = snail_frames[snail_frame_index]

            if event.type == fly_animation_timer:
                if fly_frame_index == 0:
                    fly_frame_index = 1
                else:
                    fly_frame_index = 0
                fly_surf = fly_frames[fly_frame_index]

    if game_active:  # managing state of the game
        GAME_WINDOW.blit(surface_sky, (0, 0))
        GAME_WINDOW.blit(surface_ground, (0, surface_sky.get_height()))
        title_score = display_timer()

        # player handling
        gravity_fall_speed += 1
        rect_player.y += gravity_fall_speed
        if rect_player.bottom >= surface_sky.get_height():
            rect_player.bottom = surface_sky.get_height()
        player_animation()
        GAME_WINDOW.blit(player_surf, rect_player)

        # enemy movement. Overriding this list with itself creates an enemy loop
        enemy_rect_list = enemy_movement(enemy_rect_list)

        # collisions
        game_active = collisions(rect_player, enemy_rect_list)  # solve collisions

    else:  # else menu part
        GAME_WINDOW.fill((90, 130, 160))
        GAME_WINDOW.blit(player_stand, rect_player_stand)
        enemy_rect_list.clear()  # clear enemies
        rect_player.bottom = surface_sky.get_height()  # reset player position
        gravity_fall_speed = 0
        score_msg = test_font.render(f"Your   score   is   {title_score}", False, (50, 80, 110))
        score_msg_rect = score_msg.get_rect(midtop=(WIDTH/2, 2.2*(game_title.get_height())))
        GAME_WINDOW.blit(score_msg, score_msg_rect)
        GAME_WINDOW.blit(game_title, rect_title)
        GAME_WINDOW.blit(game_hint, rect_hint)

    pygame.display.update()
    clock.tick(60)
