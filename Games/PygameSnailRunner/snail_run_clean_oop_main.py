"""This one is re-done with principles of OOP and SOLID, with sprites as new class objects."""
import pygame
from sys import exit
from random import randint, choice

pygame.init()

WIDTH = 800
HEIGHT = 460
GAME_WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('Snail Runner')
clock = pygame.time.Clock()
test_font = pygame.font.Font('font/Pixeltype.ttf', 50)
game_active = True
start_time = 0
title_score = 0

bg_music = pygame.mixer.Sound('audio/music.wav')
bg_music.set_volume(0.2)

# basic assets
surface_sky = pygame.image.load('graphics/sky.png').convert()
surface_ground = pygame.image.load('graphics/ground.png').convert()


# Player animation is a class apart from other sprites due to collision calculations
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        player_walk_1 = pygame.image.load('graphics/player/player_walk_1.png').convert_alpha()
        player_walk_2 = pygame.image.load('graphics/player/player_walk_2.png').convert_alpha()
        self.player_walk = [player_walk_1, player_walk_2]
        self.player_index = 0
        self.player_jump = pygame.image.load('graphics/player/jump.png').convert_alpha()

        self.image = self.player_walk[self.player_index]
        self.rect = self.image.get_rect(midbottom=(80, surface_sky.get_height()))
        self.gravity_fall_speed = 0

        self.jump_sound = pygame.mixer.Sound('audio/audio_jump.mp3')
        self.jump_sound.set_volume(0.2)

    def animate_state(self):
        if self.rect.bottom < surface_sky.get_height():  # jump
            self.image = self.player_jump
        else:  # walk
            self.player_index += 0.1
            if self.player_index >= len(self.player_walk):
                self.player_index = 0
            self.image = self.player_walk[int(self.player_index)]

    def gravity(self):
        self.gravity_fall_speed += 1
        self.rect.y += self.gravity_fall_speed
        if self.rect.bottom >= surface_sky.get_height():
            self.rect.bottom = surface_sky.get_height()

    def player_input(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_SPACE] and self.rect.bottom == surface_sky.get_height():
            self.gravity_fall_speed = -22
            self.jump_sound.play()

        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.rect.collidepoint(event.pos) and self.rect.bottom == surface_sky.get_height():
                self.gravity_fall_speed = - 22
                self.jump_sound.play()

    def update(self):  # separate method to apply minor methods as a whole
        self.player_input()
        self.gravity()
        self.animate_state()


class Enemy(pygame.sprite.Sprite):
    def __init__(self, category):  # category of enemy
        super().__init__()

        if category == 'fly':
            fly_frame_1 = pygame.image.load('graphics/fly/Fly1.png').convert_alpha()
            fly_frame_2 = pygame.image.load('graphics/fly/Fly2.png').convert_alpha()
            self.enemy_frames = [fly_frame_1, fly_frame_2]
            position_y = surface_sky.get_height()/2
        else:
            snail_frame_1 = pygame.image.load('graphics/snail/snail1.png').convert_alpha()
            snail_frame_2 = pygame.image.load('graphics/snail/snail2.png').convert_alpha()
            self.enemy_frames = [snail_frame_1, snail_frame_2]
            position_y = surface_sky.get_height()

        self.enemy_animation_index = 0
        self.image = self.enemy_frames[self.enemy_animation_index]
        self.rect = self.image.get_rect(midbottom=(randint(900, 2800), position_y))

    def animate_state(self):
        self.enemy_animation_index += 0.1
        if self.enemy_animation_index >= len(self.enemy_frames):
            self.enemy_animation_index = 0
        self.image = self.enemy_frames[int(self.enemy_animation_index)]

    def update(self):  # separate method to apply minor methods as a whole
        self.rect.x -= 5
        self.animate_state()
        self.vanish()

    def vanish(self):
        if self.rect.x <= -150:
            self.kill()  # easy library method to remove sprites


def display_timer():
    current_timer = int(pygame.time.get_ticks()/1500) - start_time
    surface_score = test_font.render(f"Score:{current_timer}", False, (64, 64, 64))
    rect_score = surface_score.get_rect(center=(WIDTH/2, 50))
    GAME_WINDOW.blit(surface_score, rect_score)
    return current_timer


def sprite_collision():
    # as player itself is not a sprite, but a group, bool is for collision and destruction
    if pygame.sprite.spritecollide(player.sprite, enemies_group, False):
        enemies_group.empty()  # to prevent respawning onto same enemies
        return False
    else:
        return True


# for collisions player is an instance of GroupSingle
player = pygame.sprite.GroupSingle()
player.add(Player())

enemies_group = pygame.sprite.Group()

# title screen elements
player_stand = pygame.image.load('graphics/player/player_stand.png').convert_alpha()
player_stand = pygame.transform.rotozoom(player_stand, 0.0, 2.0)
rect_player_stand = player_stand.get_rect(center=(WIDTH/2, HEIGHT/2))
game_title = test_font.render('...Snail...Runner...', False, (50, 80, 110))
rect_title = game_title.get_rect(midtop=(WIDTH/2, game_title.get_height()))
game_hint = test_font.render('Press   SPACE   to   Jump   or   to   Continue', False, (50, 80, 110))
rect_hint = game_hint.get_rect(midbottom=(WIDTH/2, HEIGHT-game_title.get_height()))

# timers - for animation
timer_obstacle = pygame.USEREVENT+1
pygame.time.set_timer(timer_obstacle, 900)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
        if game_active:
            display_timer()
        else:
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                game_active = True
                start_time = int(pygame.time.get_ticks()/1700)
        if game_active:
            if event.type == timer_obstacle:
                enemies_group.add(Enemy(choice(['snail', 'fly', 'snail', 'snail'])))

    if game_active:  # managing state of the game
        GAME_WINDOW.blit(surface_sky, (0, 0))
        GAME_WINDOW.blit(surface_ground, (0, surface_sky.get_height()))
        title_score = display_timer()
        player.draw(GAME_WINDOW)
        player.update()  # one method to run them all
        enemies_group.draw(GAME_WINDOW)
        enemies_group.update()
        bg_music.play(loops=-1)  # to loop forever with -1

        # collisions
        game_active = sprite_collision()

    else:  # else menu part
        GAME_WINDOW.fill((90, 130, 160))
        GAME_WINDOW.blit(player_stand, rect_player_stand)
        score_msg = test_font.render(f"Your   score   is   {title_score}", False, (50, 80, 110))
        score_msg_rect = score_msg.get_rect(midtop=(WIDTH/2, 2.2*(game_title.get_height())))
        GAME_WINDOW.blit(score_msg, score_msg_rect)
        GAME_WINDOW.blit(game_title, rect_title)
        GAME_WINDOW.blit(game_hint, rect_hint)

    pygame.display.update()
    clock.tick(60)
