import pygame
from sys import exit
from random import randint

pygame.init()  # initialize the library

WIDTH = 800
HEIGHT = 460

GAME_WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))  # GAME WINDOW global var
pygame.display.set_caption('Snail Runner')

clock = pygame.time.Clock()  # set up Clock to control maximum framerate
test_font = pygame.font.Font('font/Pixeltype.ttf', 50)


def display_timer():
    current_timer = int(pygame.time.get_ticks()/1500) - start_time  # easy way to round timer down
    surface_score = test_font.render(f"Score:{current_timer}", False, (64, 64, 64))
    rect_score = surface_score.get_rect(center=(WIDTH/2, 50))
    GAME_WINDOW.blit(surface_score, rect_score)
    return current_timer


def enemy_movement(enemy_list):
    if enemy_list:  # if list is empty , it`s not going to run
        for enemy_rect in enemy_list:
            # if enemy_rect.x < - 100:
            #    enemy_list.remove(enemy_rect)
            # display can be also in another function
            if enemy_rect.bottom == surface_sky.get_height():
                enemy_rect.x -= 6
                GAME_WINDOW.blit(surface_snail, enemy_rect)
            else:
                enemy_rect.x -= 3
                GAME_WINDOW.blit(surface_fly, enemy_rect)
        # list comprehension is a way to clear list gradually, after sprites go off-screen
        enemy_list = [enemy for enemy in enemy_list if enemy.x > -100]
        return enemy_list
    else:
        return []  # to be able to use append, when there is nothing in the list at the start


def collisions(player, enemies):
    if enemies:  # if there are any enemies within a list
        for enemy_rect in enemies:
            if player.colliderect(enemy_rect):
                return False  # return value for end game

    return True


surface_sky = pygame.image.load('graphics/sky.png').convert()  # converts to more pygame-friendly asset
surface_ground = pygame.image.load('graphics/ground.png').convert()
# surface_text = test_font.render('Welcome in the game', False, (255, 0, 0))

# enemies
surface_snail = pygame.image.load('graphics/snail/snail1.png').convert_alpha()  # removes transparency layers
# rect_snail = surface_snail.get_rect(bottomleft=(600, surface_sky.get_height()))
surface_fly = pygame.image.load('graphics/fly/Fly1.png').convert_alpha()  # removes transparency layers
# rect_fly = surface_fly.get_rect(bottomleft=(600, surface_sky.get_height()/2))

enemy_rect_list = []

player_stand = pygame.image.load('graphics/player/player_stand.png').convert_alpha()
# player_stand = pygame.transform.scale2x(player_stand) # easier method
# player_stand = pygame.transform.rotozoom(player_stand, 0.0, 2.0)  # another method, with rotate and filter
player_stand = pygame.transform.scale(player_stand, (2*(player_stand.get_width()), 2*(player_stand.get_height())))
surface_player = pygame.image.load('graphics/player/player_walk_1.png').convert_alpha()

game_title = test_font.render('...Snail...Runner...', False, (50, 80, 110))
rect_title = game_title.get_rect(midtop=(WIDTH/2, game_title.get_height()))

game_hint = test_font.render('Press   SPACE   to   Jump   or   to   Continue', False, (50, 80, 110))
rect_hint = game_hint.get_rect(midbottom=(WIDTH/2, HEIGHT-game_title.get_height()))

# draws rectangle around surface image, rect has 8 points to use as handles to place it :
rect_player = surface_player.get_rect(midbottom=(80, surface_sky.get_height()))
rect_player_stand = player_stand.get_rect(center=(WIDTH/2, HEIGHT/2))
gravity_fall_speed = 0
game_active = True
start_time = 0
title_score = 0

# timers for handling enemies spawning and its parameters
timer_obstacle = pygame.USEREVENT + 1  # to precisely count the events
pygame.time.set_timer(timer_obstacle, 900)  # to control a timer


while True:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()  # to avoid errors after calling quit before screen.update, and finish any code entirely

        if game_active:
            display_timer()
            if event.type == pygame.MOUSEBUTTONDOWN:  # another way to check for collisions with mouse, by events
                print(f"Mouse clicked on {event.pos}")  # check for mouse cursor position
                if rect_player.collidepoint(event.pos) and rect_player.bottom == surface_sky.get_height():
                    gravity_fall_speed = - 22

            # if event.type == pygame.MOUSEMOTION:
            #    if rect_player.collidepoint(event.pos):
            #        print("Player hovered on")

            if event.type == pygame.KEYDOWN:  # checks if any key gets pressed
                if event.key == pygame.K_SPACE and rect_player.bottom == surface_sky.get_height():
                    print("Jump")
                    gravity_fall_speed = -22
            # if event.type == pygame.KEYUP:  # checks if any key gets released
            #    print("Key up")

        else:
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                game_active = True
                start_time = int(pygame.time.get_ticks()/1700)

        if event.type == timer_obstacle and game_active:
            if randint(0, 2):
                enemy_rect_list.append(surface_snail.get_rect(bottomleft=(randint(900, 2800),
                                                                          surface_sky.get_height())))
            else:
                enemy_rect_list.append(surface_fly.get_rect(bottomleft=(600, surface_sky.get_height()/2)))

    if game_active:  # managing state of the game
        GAME_WINDOW.blit(surface_sky, (0, 0))
        GAME_WINDOW.blit(surface_ground, (0, surface_sky.get_height()))
        # GAME_WINDOW.blit(surface_text, ((WIDTH-surface_text.get_width())/2, HEIGHT/2))
        # GAME_WINDOW.blit(surface_snail, rect_snail)
        # pygame.draw.rect(GAME_WINDOW, (175, 115, 55), rect_score, 16, 5, 10, 10, 10)
        title_score = display_timer()
        # pygame.draw.line(GAME_WINDOW, (255, 0, 255), (0, 0), (WIDTH, HEIGHT), 5)
        # pygame.draw.line(GAME_WINDOW, (255, 0, 255), pygame.mouse.get_pos(), (WIDTH, HEIGHT), 5)
        # rect_snail.left -= 6
        # if rect_snail.right <= 0:
        #     rect_snail.left = WIDTH

        # player
        gravity_fall_speed += 1
        rect_player.y += gravity_fall_speed
        if rect_player.bottom >= surface_sky.get_height():  # setting the bottom
            rect_player.bottom = surface_sky.get_height()
        GAME_WINDOW.blit(surface_player, rect_player)

        # enemy movement. Overriding this list with itself creates an enemy loop
        enemy_rect_list = enemy_movement(enemy_rect_list)

        # endgame collision
        # if rect_snail.colliderect(rect_player):
        #     game_active = False
        #     rect_snail.left = WIDTH

        # collisions
        game_active = collisions(rect_player, enemy_rect_list)  # solve collisions

    else:  # else menu part
        GAME_WINDOW.fill((90, 130, 160))
        GAME_WINDOW.blit(player_stand, rect_player_stand)
        enemy_rect_list.clear()  # clear enemies
        rect_player.bottom = surface_sky.get_height()  # reset player position
        gravity_fall_speed = 0
        score_msg = test_font.render(f"Your   score   is   {title_score}", False, (50, 80, 110))
        score_msg_rect = score_msg.get_rect(midtop=(WIDTH/2, 2.2*(game_title.get_height())))
        GAME_WINDOW.blit(score_msg, score_msg_rect)
        GAME_WINDOW.blit(game_title, rect_title)
        GAME_WINDOW.blit(game_hint, rect_hint)

    # rect_player.left += 2
    # print(rect_player.left)  # nice way to measure positioning of objects for testing

    # if rect_player.colliderect(rect_snail):  # one way of calculating collisions of rects
    #    print('collision')  # other is rect.collidepoint(x,y) used a lot for mouse events

    # methods to get mouse position : 1.pygame.mouse 2.event loop checking for position with events
    # mouse_pos = pygame.mouse.get_pos()
    # if rect_player.collidepoint(mouse_pos):
    #   print(pygame.mouse.get_pressed())  # to check if any mouse button gets pressed on a collidepoint
    #   print('mouse collision')

    # one way of getting keys pressed, another is through event loop, this is good for object classes
    # keys = pygame.key.get_pressed()
    # if keys[pygame.K_SPACE]:
    #     print("Jump")

    pygame.display.update()
    clock.tick(60)  # sets up MAXIMUM framerate, MINIMUM framerate depends on hardware setup vs. game requirements
