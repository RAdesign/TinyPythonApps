"""Sudoku solver some pseudocode """


def find_empty_next(puzzle):
    # to find next row, column, on puzzle, that is not filled yet (repr as -1)
    # return a (row,column) tuple, or (None, None) - if none
    pass


def is_valid(puzzle, guess, row, col):
    # checks if the guess at (row,col) place is a valid guess (free)
    pass


def sudoku_solve(puzzle):
    # use backtracking
    # sudoku is a nested list - each inner list is a sudoku row
    # return solution
    # - choose on the puzzle to take a guess
    # - check if there is a valid input (free place)
    # - if there is a free place - make a guess 1-9
    # - check if it`s valid - if it`s - place it
    # - repeat until solved recursively
    # - if blind track - backtrack with new number
    # - if backtracking not working - puzzle unsolvable
    pass
