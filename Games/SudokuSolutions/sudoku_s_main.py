"""Sudoku solver with backtracking"""
from pprint import pprint


def find_empty_next(puzzle):
    # to find next row, column, on puzzle, that is not filled yet (repr as -1)
    # return a (row,column) tuple, or (None, None) - if none

    for r in range(9):
        for c in range(9):
            if puzzle[r][c] == -1:
                return r, c
    return None, None  # no free spaces left


def is_valid(puzzle, guess, row, col):
    # checks if the guess at (row,col) place is a valid guess (free)
    # valid guess in accordance to sudoku rules - do not be repeated in row, column, 3x3 quare
    row_values = puzzle[row]
    if guess in row_values:
        return False

    col_values = [puzzle[i][col] for i in range(9)]
    if guess in col_values:
        return False

    # the square
    row_start = (row//3)*3  # floor division to get whole num - to know if it`s in 1, 2 or 3 set of rows
    col_start = (col//3)*3

    for r in range(row_start, row_start+3):  # to get the range of each tiny square
        for c in range(col_start, col_start+3):
            if puzzle[r][c] == guess:
                return False
    return True


def sudoku_solve(puzzle):
    # use backtracking
    # sudoku is a nested list - each inner list is a sudoku row
    # return solution
    # - choose on the puzzle to take a guess
    row, col = find_empty_next(puzzle)
    # - check if there is a valid input (free place)
    if row is None:
        return True  # only if the find_empty returns None, None - no other
    # - if there is a free place - make a guess 1-9
    for guess in range(0, 10):
        # - check if it`s valid - if it`s - place it
        if is_valid(puzzle, guess, row, col):
            puzzle[row][col] = guess
            # - repeat until solved recursively
            if sudoku_solve(puzzle):
                return True
        # - if blind track - backtrack one step with new number
        puzzle[row][col] = -1
    # - if backtracking not working with all recursions - puzzle unsolvable

    return False


if __name__ == '__main__':
    test_board = [
        [3, 9, 1, -1, 5, -1, -1, -1, -1],
        [-1, -1, -1, 2, -1, -1, -1, -1, 5],
        [-1, -1, -1, 7, 1, 9, -1, 8, -1],
        [-1, 5, -1, -1, 6, 8, -1, -1, -1],
        [2, -1, 6, -1, -1, 3, -1, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, -1, 4],
        [5, -1, -1, -1, -1, -1, -1, -1, -1],
        [6, 7, -1, -1, -1, -1, -1, 4, -1],
        [1, -1, 9, -1, -1, -1, 2, -1, -1]
        ]
    test_board_2 = [
        [2, -1, -1, 6, -1, 7, 5, -1, -1],
        [-1, -1, -1, -1, -1, -1, -1, 9, 6],
        [6, -1, 7, -1, -1, 1, 3, -1, -1],
        [-1, 5, -1, 7, 3, 2, -1, -1, -1],
        [-1, 7, -1, -1, -1, -1, -1, 2, -1],
        [-1, -1, -1, 1, 8, 9, -1, 7, -1],
        [-1, -1, 3, 5, -1, -1, 6, -1, 4],
        [8, 4, -1, -1, -1, -1, -1, -1, -1],
        [-1, -1, 5, 2, -1, 6, -1, -1, 8]
    ]

    print(sudoku_solve(test_board_2))
    pprint(test_board_2)
