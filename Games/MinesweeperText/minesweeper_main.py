"""Minesweeper in text , using recursion, classes, etc. To fix : compatibility, types"""
import random
import re


class Board:
    def __init__(self, dimension_size, bombs_number):
        # basic parameters
        self.dimension_size = dimension_size
        self.bombs_number = bombs_number

        # making board
        self.board = self.create_new_board()
        self.board_assign_values()

        self.dug = set()  # a set to keep track of explored locations

    def create_new_board(self):
        # list of lists with None
        board: list = [[None for _ in range(self.dimension_size)] for _ in range(self.dimension_size)]

        # planting bombs
        bombs_planted = 0
        while bombs_planted < self.bombs_number:
            location = random.randint(0, self.dimension_size**2 - 1)  # random int from board spots range
            row = location // self.dimension_size  # floor division for range of acceptable rows
            col = location % self.dimension_size  # modulo for range of acceptable columns

            if board[row][col] == '*':  # recognizing a string bomb has already been planted
                continue  # until proper location found

            board[row][col] = '*'  # planting string bomb
            bombs_planted += 1

        return board

    def board_assign_values(self):
        # after planting the bombs - time for clues. every spot will have 0-8 hint of near bombs
        for r in range(self.dimension_size):
            for c in range(self.dimension_size):
                if self.board[r][c] == '*':
                    continue  # it was a bomb
                self.board[r][c] = self.get_bomb_hints(r, c)

    def get_bomb_hints(self, row, col):
        # iterate every position within boundaries
        # top: (row-1,col-1) (row-1,col) (row-1,col+1)
        # middle: (row,col-1) (row,col+1)
        # bottom: (row+1,col-1) (row+1,col) (row+1,col+1)
        bomb_hints = 0
        for r in range(max(0, row-1), min(self.dimension_size-1, row+1)+1):
            for c in range(max(0, col-1), min(self.dimension_size-1, col+1)+1):
                if r == row and c == col:
                    continue  # it`s our dugout location, pass this
                if self.board[r][c] == '*':
                    bomb_hints += 1

        return bomb_hints

    def dugout(self, row, col):
        # dig at position, return bool for clean or bomb
        self.dug.add((row, col))  # tracking already dugout

        if self.board[row][col] == '*':
            return False
        elif self.board[row][col] > 0:
            return True

        for r in range(max(0, row-1), min(self.dimension_size-1, row+1)+1):
            for c in range(max(0, col-1), min(self.dimension_size-1, col+1)+1):
                if (r, c) in self.dug:
                    continue
                self.dugout(r, c)
        return True

    def __str__(self):
        # returns a string showing board
        # an array of values:
        visible_board = [[None for _ in range(self.dimension_size)] for _ in range(self.dimension_size)]
        for row in range(self.dimension_size):
            for col in range(self.dimension_size):
                if (row, col) in self.dug:
                    visible_board[row][col] = str(self.board[row][col])
                else:
                    visible_board[row][col] = ' '
        # array to string representation:
        string_rep = ''
        widths = []  # max column w for printing
        for indx in range(self.dimension_size):
            columns = map(lambda x: x[indx], visible_board)
            widths.append(len(max(columns, key=len)))
        # print the strings
        indices = [i for i in range(self.dimension_size)]
        indices_row = '   '
        cells = []
        for indx, col in enumerate(indices):
            form = '%-' + str(widths[indx]) + "s"
            cells.append(form % col)
        indices_row += '   '.join(cells)
        indices_row += '   \n'

        for i in range(len(visible_board)):
            row = visible_board[i]
            string_rep += f'{i} |'
            cells = []
            for indx, col in enumerate(row):
                form = '%-' + str(widths[indx]) + "s"
                cells.append(form % col)
            indices_row += '  |'.join(cells)
            indices_row += '  |\n'

        str_len = int(len(string_rep)/self.dimension_size)
        string_rep = indices_row + '-'*str_len + '\n' + string_rep + '-'*str_len

        return string_rep


def play_game(dimension_size=10, bombs_number=10):

    board = Board(dimension_size, bombs_number)
    safe = True

    while len(board.dug) < board.dimension_size ** 2 - bombs_number:
        print(board)

        user_input = re.split(',(\\s)*', input("Where to dig? Input: row,col"))
        row, col = int(user_input[0]), int(user_input[-1])
        if row < 0 or row >= board.dimension_size or col < 0 or col >= dimension_size:
            print("Invalid input, try again.")
            continue
        safe = board.dugout(row, col)  # location valid, so dig
        if not safe:  # a string bomb
            break

    if safe:
        print("That was the last one, you won!")
    else:
        print("You die !")
        board.dug = [(r, c) for r in range(board.dimension_size) for c in range(board.dimension_size)]
        print(board)


if __name__ == '__main__':
    play_game()