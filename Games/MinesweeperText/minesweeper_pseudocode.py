"""pseudocode rules with basic classes and methods for minesweeper"""
import random
import re

# board is a basic class


class Board:
    def __init__(self, dimension_size, bombs_number):
        # basic parameters
        self.dimension_size = dimension_size
        self.bombs_number = bombs_number

        # making board
        self.board = self.create_new_board()
        self.board_assign_values()

        self.dugout = set()  # a set to keep track of explored locations

    def create_new_board(self):
        # make a new board based on parameters at init
        # it will be a list of lists
        return [[]]  # of lists

    def board_assign_values(self):
        # after planting the bombs - time for clues. every spot will have 0-8 hint of near bombs
        pass

    def get_bomb_hints(self , row, col):
        # iterate every position within boundaries
        # top: (row-1,col-1) (row-1,col) (row-1,col+1)
        # middle: (row,col-1) (row,col+1)
        # bottom: (row+1,col-1) (row+1,col) (row+1,col+1)
        pass

    def dugout(self, row, col):
        # dig at position, return bool for clean or bomb
        pass

    def __str__(self):
        # returns a string showing board
        return ''


def play_game(dimension_size=10, bombs_number=12):
    # create board, plant bombs
    # print board, ask where to dig
    # bool for effect - end game or continue, with recursive dig of one square next to successful
    # repeat for bool win or not
    pass


if __name__ == '__main__':
    play_game()