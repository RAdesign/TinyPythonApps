import pygame
import os
import random
pygame.font.init()


WIDTH = 800
HEIGHT = 600
GAME_WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Space Shooters")


SHIP_YELLOW = pygame.image.load(os.path.join('assets', 'pixel_ship_yellow.png'))
SMALL_SHIP_RED = pygame.image.load(os.path.join('assets', 'pixel_ship_red_small.png'))
SMALL_SHIP_GREEN = pygame.image.load(os.path.join('assets', 'pixel_ship_green_small.png'))
SMALL_SHIP_BLUE = pygame.image.load(os.path.join('assets', 'pixel_ship_blue_small.png'))
LASER_YELLOW = pygame.image.load(os.path.join('assets', 'pixel_laser_yellow.png'))
LASER_RED = pygame.image.load(os.path.join('assets', 'pixel_laser_red.png'))
LASER_GREEN = pygame.image.load(os.path.join('assets', 'pixel_laser_green.png'))
LASER_BLUE = pygame.image.load(os.path.join('assets', 'pixel_laser_blue.png'))
BACKGROUND_BLACK = pygame.transform.scale(pygame.image.load(
    os.path.join('assets', 'background_black.png')), (WIDTH, HEIGHT))


class Ship:
    def __init__(self, x, y, health=100):
        self.x = x
        self.y = y
        self.health = health
        self.ship_img = None
        self.laser_img = None
        self.lasers = []
        self.cooldown_counter = 0
        self.cooldown = 50

    def draw(self, window):
        window.blit(self.ship_img, (self.x, self.y))
        # pygame.draw.rect(window, (255, 255, 255), (self.x, self.y, 50, 50))  # for testing
        for laser in self.lasers:
            laser.draw(window)

    def move_laser(self, velocity, obj):  # obj to check for hitting player (general class)
        self.cooldown_counting()
        for laser in self.lasers:
            laser.laser_move(velocity)
            if laser.laser_off_screen(HEIGHT):
                self.lasers.remove(laser)
            elif laser.collision(obj):
                obj.health -= 10
                self.lasers.remove(laser)

    def cooldown_counting(self):  # resetting and increasing cooldown of a laser
        if self.cooldown_counter >= self.cooldown:
            self.cooldown_counter = 0
        elif self.cooldown_counter > 0:
            self.cooldown_counter += 1

    def shoot(self):
        if self.cooldown_counter == 0:
            laser = Laser(self.x, self.y, self.laser_img)
            self.lasers.append(laser)
            self.cooldown_counter = 1

    def get_width(self):
        return self.ship_img.get_width()

    def get_height(self):
        return self.ship_img.get_height()


class Player(Ship):
    def __init__(self, x, y, health=100):
        super().__init__(x, y, health)
        self.ship_img = SHIP_YELLOW
        self.laser_img = LASER_YELLOW
        self.mask = pygame.mask.from_surface(self.ship_img)  # mask for pixel perfect hitbox
        self.max_hp = health

    # overriding general Ship method
    def move_laser(self, velocity, objs):  # objs to check for hitting enemies
        self.cooldown_counting()
        for laser in self.lasers:
            laser.laser_move(velocity)
            if laser.laser_off_screen(HEIGHT):
                self.lasers.remove(laser)
            else:
                for obj in objs:
                    if laser.collision(obj):
                        objs.remove(obj)
                        if laser in self.lasers:
                            self.lasers.remove(laser)

    def draw(self, window):
        super().draw(window)
        self.healthbar(window)

    def healthbar(self, window):  # double decreasing healthbar
        pygame.draw.rect(window,(0,255,0),(self.x,self.y+self.ship_img.get_height()+10,self.ship_img.get_width(), 10))
        pygame.draw.rect(window,(255,0,0),(self.x,self.y+self.ship_img.get_height()+10, self.ship_img.get_width() * ((self.max_hp - self.health)/self.max_hp), 10))


class Enemy(Ship):
    # map dictionary to pass color choice as a string
    COLOR_MAP = {
        "red": (SMALL_SHIP_RED, LASER_RED),
        "green": (SMALL_SHIP_GREEN, LASER_GREEN),
        "blue": (SMALL_SHIP_BLUE, LASER_BLUE)
    }

    def __init__(self, x, y, color, health=100):
        super().__init__(x, y, health)
        self.ship_img, self.laser_img = self.COLOR_MAP[color]
        self.mask = pygame.mask.from_surface(self.ship_img)

    def enemy_move(self, velocity):
        self.y += velocity

    def shoot(self):  # override to correct center of laser shots
        if self.cooldown_counter == 0:
            laser = Laser(self.x-15, self.y, self.laser_img)
            self.lasers.append(laser)
            self.cooldown_counter = 1


class Laser:
    def __init__(self, x, y, laser_image):
        self.x = x
        self.y = y
        self.laser_image = laser_image
        self.mask = pygame.mask.from_surface(self.laser_image)

    def draw(self, window):
        window.blit(self.laser_image, (self.x, self.y))

    def laser_move(self, laser_velocity):
        self.y += laser_velocity  # for moving another direction just add negative value

    def laser_off_screen(self, height):
        return not(0 <= self.y < height)

    def collision(self, obj):
        return collide(self, obj)  # returns (T/F) of collide, checks if collides with itself


def collide(object_1, object_2):  # how to calculate sprite objects collisions
    offset_x = object_2.x - object_1.x
    offset_y = object_2.y - object_1.y
    # with overlap method, using both objects masks, and with the offset
    return object_1.mask.overlap(object_2.mask, (offset_x, offset_y)) is not None


def main():
    run = True
    fps = 60  # refresh rate for events / second
    level = 0
    lives = 5
    main_font = pygame.font.SysFont('arial', 20)
    lost_font = pygame.font.SysFont('arial', 50)

    enemy_ships = []
    enemy_ships_wave = 5
    enemy_ships_velocity = 1

    player_velocity = 5  # to mark the speed , how fast the ships are moving (pixels, clock speed)
    laser_velocity = 7

    player_ship = Player((WIDTH-50)//2, HEIGHT-100)

    clock = pygame.time.Clock()

    player_lost = False

    lost_count = 0

    def redraw_window():  # function scope within main
        GAME_WINDOW.blit(BACKGROUND_BLACK, (0, 0))
        level_label = main_font.render(f"Level: {level}", True, (255, 255, 255))
        lives_label = main_font.render(f"Lives: {lives}", True, (255, 255, 255))

        GAME_WINDOW.blit(lives_label, (10, 10))
        GAME_WINDOW.blit(level_label, (WIDTH-level_label.get_width()-10, 10))

        for enemy in enemy_ships:
            enemy.draw(GAME_WINDOW)

        player_ship.draw(GAME_WINDOW)

        if player_lost:
            lost_label = lost_font.render("You have lost The Game!", True, (255, 255, 255))
            GAME_WINDOW.blit(lost_label, (WIDTH/2 - lost_label.get_width()/2, 400))

        pygame.display.update()  # display refresh with fps rate

    while run:
        clock.tick(fps)  # to keep consistency for every computer
        redraw_window()

        if lives <= 0 or player_ship.health <= 0:
            player_lost = True
            lost_count += 1
        if player_lost:
            if lost_count > fps*4:  # for how long the lost message will appear
                run = False
            else:
                continue  # to stop the game for while after loosing, and then quit

        if len(enemy_ships) == 0:
            level += 1
            enemy_ships_wave += 3
            for item in range(enemy_ships_wave):
                enemy_ship = Enemy(random.randrange(50, WIDTH-100), random.randrange(-1600, -100),
                                   random.choice(["red", "blue", "green"]))
                enemy_ships.append(enemy_ship)

        for event in pygame.event.get():  # event loop to run all events
            if event.type == pygame.QUIT:
                # run = False
                quit()

        keys = pygame.key.get_pressed()  # initialize entire key dictionary
        if keys[pygame.K_a] and player_ship.x + player_velocity > 0:   # left
            player_ship.x -= player_velocity
        if keys[pygame.K_d] and player_ship.x + player_velocity + player_ship.get_width() < WIDTH:  # right
            player_ship.x += player_velocity
        if keys[pygame.K_w] and player_ship.y - player_velocity > 0:  # up
            player_ship.y -= player_velocity
        if keys[pygame.K_s] and player_ship.y + player_velocity + player_ship.get_height()+10 < HEIGHT:   # down
            player_ship.y += player_velocity
        if keys[pygame.K_SPACE]:
            player_ship.shoot()

        for enemy_ship in enemy_ships[:]:
            enemy_ship.enemy_move(enemy_ships_velocity)
            enemy_ship.move_laser(laser_velocity, player_ship)

            if random.randrange(0, 240) == 1:  # prob. triggers every number/fps seconds
                enemy_ship.shoot()

            if collide(enemy_ship, player_ship):
                player_ship.health -= 10
                enemy_ships.remove(enemy_ship)
            elif enemy_ship.y + enemy_ship.get_height() > HEIGHT:
                lives -= 1
                enemy_ships.remove(enemy_ship)

        player_ship.move_laser(-laser_velocity, enemy_ships)  # negative vel, to show player laser


def main_menu():  # simple mouse trigger menu for starting the game
    title_font = pygame.font.SysFont("arial", 40)
    run = True
    while run:
        GAME_WINDOW.blit(BACKGROUND_BLACK, (0, 0))
        title_label = title_font.render("Press mouse button to start the game...", True, (255, 255, 255))
        GAME_WINDOW.blit(title_label, (WIDTH/2-title_label.get_width()/2, 350))
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                main()
    pygame.quit()


main_menu()

if __name__ == "__main__":
    main()
