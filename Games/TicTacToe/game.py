import math
import time
from player import HumanPlayer, RandomCompPlayer


class TicTacToe:
    def __init__(self):
        self.board = self.make_board()
        self.current_winner = None  # tracking winner

    @staticmethod
    def make_board():
        return [' ' for _ in range(9)]  # single list for 3x3 board

    def print_board(self):
        for row in [self.board[i*3:(i+1)*3] for i in range(3)]:  # row indices representation
            print('| '+' | '.join(row) + ' |')

    @staticmethod
    def print_board_numbers():  # which number goes to what box
        number_board = [[str(i) for i in range(j*3, (j+1)*3)] for j in range(3)]
        for row in number_board:
            print('| ' + ' | '.join(row) + ' |')

    def make_move(self, square, letter):  # if valid, make move (square to letter assign), return True. Not - False
        if self.board[square] == ' ':
            self.board[square] = letter  # square to letter assignment
            if self.is_winner(square, letter):  # checks for winner
                self.current_winner = letter
            return True
        return False

    def is_winner(self, square, letter):  # checks for 3 in a row anywhere - rows, columns, diagonals
        row_index = math.floor(square//3)  # division to whole numbers
        row = self.board[row_index*3:(row_index + 1)*3]  # list of items in selected row
        if all([spot == letter for spot in row]):  # checks rows
            return True
        col_index = square % 3  # division rest for columns
        column = [self.board[col_index+i*3] for i in range(3)]  # checks columns
        if all([spot == letter for spot in column]):
            return True

        # checking diagonals is only possible for 0,2,4,6,8 indices of squares in a list
        if square % 2 == 0:
            diagonal_1 = [self.board[i] for i in [0, 4, 8]]
            if all([spot == letter for spot in diagonal_1]):
                return True
            diagonal_2 = [self.board[i] for i in [2, 4, 6]]
            if all([spot == letter for spot in diagonal_2]):
                return True
        # otherwise we have False, so a draw
        return False

    def available_moves(self):  # returns []
        # another way - list comprehension:
        return [i for i, spot in enumerate(self.board) if spot == ' ']
        # moves = []
        # for (i, spot) in enumerate(self.board):  # creates list, assigns tuples (index, value)
        #     if spot == ' ':  # empty space
        #         moves.append(i)  # append spot index to mark move as done
        # return moves

    def empty_squares(self):  # checks itself for empty spaces in a board
        return ' ' in self.board  # it`s a bool

    def how_many_empty(self):
        # return len(self.available_moves())
        return self.board.count(' ')  # board is a list so count works


def play_game(game, x_pl, o_pl, print_game):
    # returns winner(letter) , or None for a tie
    if print_game:
        game.print_board_numbers()

    letter = 'X'  # starting letter
    # while loop iterating until there is an empty square
    while game.empty_squares():
        if letter == 'O':  # who gets moving
            square = o_pl.get_move(game)
        else:
            square = x_pl.get_move(game)

        if game.make_move(square, letter):
            if print_game:
                print(letter + 'makes a move to square {}'.format(square))
                game.print_board()  # refresh state print a new
                print('')  # next line

            if game.current_winner:
                if print_game:
                    print(letter + ' wins !')
                return letter

            # letter alternation after move, to switch players
            letter = 'O' if letter == 'X' else 'X'
        time.sleep(0.7)

    if print_game:
        print('It`s a draw.')


if __name__ == '__main__':
    x_player = RandomCompPlayer('X')
    o_player = HumanPlayer('O')
    t_game = TicTacToe()
    play_game(t_game, x_player, o_player, print_game=True)
