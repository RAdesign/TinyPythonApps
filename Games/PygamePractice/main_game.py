import pygame
import os  # for paths to assets
pygame.font.init()
pygame.mixer.init()

pygame.display.set_caption("Shooter game.")
WIDTH = 900
HEIGHT = 480
GAME_WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
WHITE_COLOR = (255, 255, 255)
BLACK_COLOR = (0, 0, 0)
RED_COLOR = (255, 0, 0)
YELLOW_COLOR = (255, 255, 0)
BORDER = pygame.Rect((WIDTH-5)//2, 0, 10, HEIGHT)  # // double division for floating point issue

BULLET_SOUND_HIT = pygame.mixer.Sound(os.path.join('assets', 'bullet_hit.mp3'))
BULLET_SOUND = pygame.mixer.Sound(os.path.join('assets', 'bullet_fire.mp3'))

HITPOINTS_FONT = pygame.font.SysFont('arial', 20)
WIN_FONT = pygame.font.SysFont('comicsans', 30)

FPS = 60
VELOCITY = 5
BULLET_VELOCITY = 7
BULLETS_LIMIT = 10
SHIP_WIDTH, SHIP_HEIGHT = 55, 40
YELLOW_HIT = pygame.USEREVENT + 1  # custom event handles hit, +1 is event unique id
RED_HIT = pygame.USEREVENT + 2  # so this one has to have +2 as identifier
YELLOW_SHIP_IMAGE = pygame.image.load(os.path.join('assets', 'spaceship_yellow.png'))
YELLOW_SHIP = pygame.transform.rotate(pygame.transform.scale(YELLOW_SHIP_IMAGE, (SHIP_WIDTH, SHIP_HEIGHT)), 90)
RED_SHIP_IMAGE = pygame.image.load(os.path.join('assets', 'spaceship_red.png'))
RED_SHIP = pygame.transform.rotate(pygame.transform.scale(RED_SHIP_IMAGE, (SHIP_WIDTH, SHIP_HEIGHT)), 270)
SPACE_BACKGROUND = pygame.transform.scale(pygame.image.load(os.path.join('assets', 'space.png')), (WIDTH, HEIGHT))


def draw_window(red_box, yellow_box, red_bullets, yellow_bullets, red_hitpoints, yellow_hitpoints):
    GAME_WINDOW.blit(SPACE_BACKGROUND, (0, 0))
    pygame.draw.rect(GAME_WINDOW, BLACK_COLOR, BORDER)
    red_hp_text = HITPOINTS_FONT.render(f"HP: {str(red_hitpoints)}", True, WHITE_COLOR)
    yellow_hp_text = HITPOINTS_FONT.render(f"HP: {str(yellow_hitpoints)}", True, WHITE_COLOR)
    GAME_WINDOW.blit(red_hp_text, (WIDTH - red_hp_text.get_width()-10, 10))
    GAME_WINDOW.blit(yellow_hp_text, (10, 10))
    GAME_WINDOW.blit(YELLOW_SHIP, (yellow_box.x, yellow_box.y))
    GAME_WINDOW.blit(RED_SHIP, (red_box.x, red_box.y))

    for bullet in red_bullets:
        pygame.draw.rect(GAME_WINDOW, RED_COLOR, bullet)
    for bullet in yellow_bullets:
        pygame.draw.rect(GAME_WINDOW, YELLOW_COLOR, bullet)

    pygame.display.update()


def handle_mov_yellow(keys_pressed, yellow_box):
    if keys_pressed[pygame.K_a] and yellow_box.x - VELOCITY > 0:  # left
        yellow_box.x -= VELOCITY
    if keys_pressed[pygame.K_w] and yellow_box.y - VELOCITY > 0:  # up
        yellow_box.y -= VELOCITY
    if keys_pressed[pygame.K_d] and yellow_box.x + VELOCITY + yellow_box.width < BORDER.x:  # right
        yellow_box.x += VELOCITY
    if keys_pressed[pygame.K_s] and yellow_box.y + VELOCITY + yellow_box.height < HEIGHT-15:  # down
        yellow_box.y += VELOCITY


def handle_mov_red(keys_pressed, red_box):
    if keys_pressed[pygame.K_LEFT] and red_box.x + VELOCITY - red_box.width > BORDER.x-20:  # left
        red_box.x -= VELOCITY
    if keys_pressed[pygame.K_UP] and red_box.y - VELOCITY > 0:  # up
        red_box.y -= VELOCITY
    if keys_pressed[pygame.K_RIGHT] and red_box.x + VELOCITY + red_box.width < WIDTH+15:  # right
        red_box.x += VELOCITY
    if keys_pressed[pygame.K_DOWN] and red_box.y + VELOCITY + red_box.height < HEIGHT-15:  # down
        red_box.y += VELOCITY


def handle_bullets(yellow_bullets, red_bullets, yellow_box, red_box):
    for bullet in yellow_bullets:
        bullet.x += BULLET_VELOCITY
        if red_box.colliderect(bullet):
            pygame.event.post(pygame.event.Event(RED_HIT))
            yellow_bullets.remove(bullet)
        elif bullet.x > WIDTH:  # not to remove bullet twice
            yellow_bullets.remove(bullet)

    for bullet in red_bullets:
        bullet.x -= BULLET_VELOCITY
        if yellow_box.colliderect(bullet):
            pygame.event.post(pygame.event.Event(YELLOW_HIT))
            red_bullets.remove(bullet)
        elif bullet.x < 0:  # not to remove bullet twice
            red_bullets.remove(bullet)


def draw_winner(text):
    draw_text = WIN_FONT.render(text, True, WHITE_COLOR)
    GAME_WINDOW.blit(draw_text, (WIDTH//2 - draw_text.get_width()/2, HEIGHT/2 - draw_text.get_height()/2))
    pygame.display.update()
    pygame.time.delay(6000)


def main():
    red_box = pygame.Rect(700, 200, SHIP_WIDTH, SHIP_HEIGHT)
    yellow_box = pygame.Rect(200, 200, SHIP_WIDTH, SHIP_HEIGHT)

    red_bullets = []
    yellow_bullets = []

    red_hitpoints = 3
    yellow_hitpoints = 3

    clock = pygame.time.Clock()
    run = True
    while run:
        clock.tick(FPS)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()  # this has to be here if we re-launch main() at the end

            if event.type == pygame.KEYDOWN and len(yellow_bullets) < BULLETS_LIMIT:
                if event.key == pygame.K_LCTRL:
                    bullet = pygame.Rect(yellow_box.x + yellow_box.width, yellow_box.y + yellow_box.height//2-2, 10, 5)
                    yellow_bullets.append(bullet)
                    BULLET_SOUND.play()

                if event.key == pygame.K_RCTRL and len(red_bullets) < BULLETS_LIMIT:
                    bullet = pygame.Rect(red_box.x, red_box.y + red_box.height//2-2, 10, 5)
                    red_bullets.append(bullet)
                    BULLET_SOUND.play()

            if event.type == RED_HIT:
                red_hitpoints -= 1
                BULLET_SOUND_HIT.play()

            if event.type == YELLOW_HIT:
                yellow_hitpoints -= 1
                BULLET_SOUND_HIT.play()

        keys_pressed = pygame.key.get_pressed()
        handle_mov_yellow(keys_pressed, yellow_box)
        handle_mov_red(keys_pressed, red_box)

        handle_bullets(yellow_bullets, red_bullets, yellow_box, red_box)
        # yellow_box.x += 1
        draw_window(red_box, yellow_box, red_bullets, yellow_bullets, red_hitpoints, yellow_hitpoints)

        win_txt = ""
        if red_hitpoints <= 0:
            win_txt = "Yellow wins !"
        if yellow_hitpoints <= 0:
            win_txt = "Red wins !"
        if win_txt != "":
            draw_winner(win_txt)
            break

    # pygame.quit() for auto quit without repeating a game
    main()  # for re-launching a game


if __name__ == "__main__":
    main()
