"""Player class - 1. shows an image of the player, 2. moves the player, 3.constrains player to window,
4. shooting and recharge mechanics of laser"""
import pygame.sprite
from .laser import Laser  # this '.laser' to help Python to find right module properly


class Player(pygame.sprite.Sprite):
    def __init__(self, position, constraint_x, speed):
        super().__init__()
        self.image = pygame.image.load('./graphics/player.png').convert_alpha()  # get rid of alpha channel
        self.rect = self.image.get_rect(midbottom=position)
        self.speed = speed
        self.max_constraint_x = constraint_x
        self.ready = True  # ready to shoot after init, and after cooldown
        self.laser_time = 0
        self.laser_cooldown = 600  # time to cooldown
        self.lasers = pygame.sprite.Group()  # to put instances of Laser into

        self.laser_sound = pygame.mixer.Sound('./audio/audio_laser.wav')
        self.laser_sound.set_volume(0.2)

    def get_input(self):
        keys = pygame.key.get_pressed()

        if keys[pygame.K_RIGHT]:
            self.rect.x += self.speed
        elif keys[pygame.K_LEFT]:
            self.rect.x -= self.speed

        elif keys[pygame.K_SPACE] and self.ready:
            self.shoot_laser()
            self.ready = False
            # this get ticks is used once we shoot, as timeframe, with condition ready = False
            self.laser_time = pygame.time.get_ticks()
            self.laser_sound.play(0)

    def recharge_laser(self):
        if not self.ready:  # checks for condition ready = False to start separate counter
            current_time = pygame.time.get_ticks()  # this get ticks is run continuously
            if current_time - self.laser_time >= self.laser_cooldown:  # checks counters up with cooldown value
                self.ready = True

    def constraint_move(self):
        if self.rect.left <= 0:
            self.rect.left = 0
        if self.rect.right >= self.max_constraint_x:
            self.rect.right = self.max_constraint_x

    def shoot_laser(self):
        self.lasers.add(Laser(self.rect.center, 5, self.rect.bottom))  # player bottom works as screen height
        # print("PEW, PEW, PEW")
        # every shot a new instance is created at player position

    def update(self):
        self.get_input()
        self.constraint_move()
        self.recharge_laser()
        self.lasers.update()
