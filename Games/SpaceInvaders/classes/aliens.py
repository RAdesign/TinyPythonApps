"""Alien class. Will be spread with help of enumerate. """
import pygame


class Alien(pygame.sprite.Sprite):
    def __init__(self, color, x, y):
        super().__init__()
        file_path = './graphics/' + color + '.png'  # way to get any color image
        self.image = pygame.image.load(file_path).convert_alpha()
        self.rect = self.image.get_rect(topleft=(x, y))

        match color:
            case 'red':
                self.points = 200
            case 'green':
                self.points = 50
            case 'yellow':
                self.points = 100

    def update(self, direction):
        self.rect.x += direction


class SuperAlien(pygame.sprite.Sprite):
    def __init__(self, screen_side, screen_width):
        super().__init__()
        self.image = pygame.image.load('./graphics/extra.png').convert_alpha()
        if screen_side == 'right':
            x = screen_width + 60
            self.speed = -2
        else:
            x = -60
            self.speed = 2

        self.rect = self.image.get_rect(topleft=(x, 60))

    def update(self):
        self.rect.x += self.speed
