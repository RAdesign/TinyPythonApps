"""Laser class, a sprite with position and speed. With key pressed - spawns laser on player position,
and moves upwards. Also - for Alien lasers, but moving it downwards."""
import pygame


class Laser(pygame.sprite.Sprite):
    def __init__(self, position, speed, screen_height, color='green'):  # default is simple enough to puy here
        super().__init__()
        self.image = pygame.Surface((4, 20))
        self.color = color
        self.image.fill(self.color)
        self.rect = self.image.get_rect(center=position)
        self.speed = speed
        self.height_constraint_y = screen_height

    def destroy(self):
        if self.rect.y <= -60 or self.rect.y >= self.height_constraint_y + 60:
            self.kill()

    def update(self):
        self.rect.y -= self.speed
        self.destroy()
