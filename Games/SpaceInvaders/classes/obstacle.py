"""Obstacle class - each is made of 'blocks of sprites', drawing it with enum method, using rows of strings
 filling each place if there is a certain character at coordinates"""
import pygame


class Block(pygame.sprite.Sprite):
    def __init__(self, size, color, x, y):
        super().__init__()
        self.image = pygame.Surface((size, size))
        self.image.fill(color)
        self.rect = self.image.get_rect(topleft=(x, y))  # here x,y to be able to draw and destroy easily


obstacle_shape = [
    '  xxxxxxx',
    ' xxxxxxxxx',
    'xxxxxxxxxxx',
    'xxxxxxxxxxx',
    'xxxxxxxxxxx',
    'xxx     xxx',
    'xx       xx',
]
