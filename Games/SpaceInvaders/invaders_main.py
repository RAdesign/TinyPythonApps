"""Space invaders tiny game. Tiny pygame exercise. TO DO : extended menu, difficulty level choice, " \
music choice, sound choice, screen size choice, alien amount, speed, lasers frequency """
import pygame
import sys
from random import choice, randint

import pygame.display

from classes.player import Player
from classes.obstacle import obstacle_shape, Block
from classes.aliens import Alien, SuperAlien
from classes.laser import Laser


class Game:  # class to contain framework of the entire game and complete
    def __init__(self):
        # player
        player_sprite = Player((SCREEN_WIDTH/2, SCREEN_HEIGHT), SCREEN_WIDTH, 5)
        self.player = pygame.sprite.GroupSingle(player_sprite)  # PyCharm shows strange error

        # menu , HP, points
        self.lives = 3
        self.lives_surface = pygame.image.load('./graphics/player.png').convert_alpha()
        self.lives_x_start_position = SCREEN_WIDTH - (self.lives_surface.get_width() * 2 + 20)
        self.score = 0
        self.font = pygame.font.SysFont('arial', 20)

        # obstacle
        self.shape = obstacle_shape
        self.block_size = 6
        self.blocks = pygame.sprite.Group()
        # how many obstacles (for list - 0,1,2,3)
        self.obstacle_amount = 4
        # getting position of each obstacle, depending on the amount
        self.obstacle_x_spread = [num * (SCREEN_WIDTH/self.obstacle_amount) for num in range(self.obstacle_amount)]
        # first values for offset for each obstacle, *to unpack this list on the fly
        self.make_multiply_obstacles(*self.obstacle_x_spread, x_start=SCREEN_WIDTH/14, y_start=480)

        # aliens
        self.aliens = pygame.sprite.Group()
        self.aliens_setup(rows=7, cols=9)
        self.alien_direction = 1
        self.alien_lasers = pygame.sprite.Group()

        # super alien
        self.super_alien = pygame.sprite.GroupSingle()
        self.super_alien_spawn_time = randint(1200, 2400)

        # music and audio
        music = pygame.mixer.Sound('./audio/music.wav')
        music.set_volume(0.1)
        music.play(loops=-1)
        self.laser_sound = pygame.mixer.Sound('./audio/audio_laser.wav')
        self.laser_sound.set_volume(0.1)
        self.explosion_sound = pygame.mixer.Sound('./audio/audio_explosion.wav')
        self.explosion_sound.set_volume(0.3)

    def create_obstacle(self, x_start, y_start, offset_x):
        # enumerate returns index, value - a crucial feature here
        for row_index, row in enumerate(self.shape):
            for col_index, col in enumerate(row):
                if col == 'x':
                    x = x_start + col_index * self.block_size + offset_x  # drawing by block size
                    y = y_start + row_index * self.block_size
                    block = Block(self.block_size, (240, 80, 80), x, y)
                    self.blocks.add(block)

    def make_multiply_obstacles(self, *offset, x_start, y_start):  # unpacking operator for unspecified no. of obstacles
        for offset_x in offset:  # iterate through all arguments passed to *
            self.create_obstacle(x_start, y_start, offset_x)

    def aliens_setup(self, rows, cols, x_distance=60, y_distance=-48, x_offset=40, y_offset=30):
        for row_index, row in enumerate(range(rows)):  # rows and cols specify how many aliens
            for col_index, col in enumerate(range(cols)):
                x = x_offset + col_index * x_distance
                y = y_offset + row_index * y_distance
                if row_index % 2:
                    alien_sprite = Alien('yellow', x, y)
                elif row_index % 3:
                    alien_sprite = Alien('green', x, y)
                else:
                    alien_sprite = Alien('red', x, y)
                self.aliens.add(alien_sprite)

    def alien_position_check(self):
        all_aliens = self.aliens.sprites()  # to get all aliens in one list
        for alien in all_aliens:  # to check for each alien in group, as they will be destroyed
            if alien.rect.right >= SCREEN_WIDTH:
                self.alien_direction = -1
                self.aliens_move_down(1)
            elif alien.rect.left <= 0:
                self.alien_direction = 1
                self.aliens_move_down(1)

    def aliens_move_down(self, distance):
        if self.aliens:   # checks if there are any aliens, to prevent errors
            for alien in self.aliens.sprites():
                alien.rect.y += distance

    def alien_shoot_laser(self):
        if self.aliens.sprites():   # if there are any alien sprites present
            random_alien = choice(self.aliens.sprites())
            laser_sprite = Laser(random_alien.rect.center, -5, SCREEN_HEIGHT, 'white')
            self.alien_lasers.add(laser_sprite)
            self.laser_sound.play(0)

    def super_alien_spawn(self):  # not a ticker spawn, but more conventional
        self.super_alien_spawn_time -= 1  # decreasing timer
        if self.super_alien_spawn_time <= 0:
            self.super_alien.add(SuperAlien(choice(['left', 'right']), SCREEN_WIDTH))
            self.super_alien_spawn_time = randint(800, 1600)

    def check_collisions(self):
        # with player lasers
        if self.player.sprite.lasers:
            for laser in self.player.sprite.lasers:
                # obstacle collision
                if pygame.sprite.spritecollide(laser, self.blocks, True):
                    laser.kill()
                # alien collision
                aliens_got_hit = pygame.sprite.spritecollide(laser, self.aliens, True)
                if aliens_got_hit:
                    for alien in aliens_got_hit:
                        self.score += alien.points
                    laser.kill()
                    self.explosion_sound.play(0)

                # version for no score differences:
                # if pygame.sprite.spritecollide(laser, self.aliens, True):
                #    laser.kill()
                # super alien collision
                if pygame.sprite.spritecollide(laser, self.super_alien, True):
                    laser.kill()
                    self.explosion_sound.play(0)
                    self.score += 500
        # with alien lasers
        if self.alien_lasers:
            for laser in self.alien_lasers:
                # obstacles
                if pygame.sprite.spritecollide(laser, self.blocks, True):
                    laser.kill()
                # player
                if pygame.sprite.spritecollide(laser, self.player, False):
                    laser.kill()
                    self.lives -= 1
                    if self.lives <= 0:
                        pygame.quit()
                        sys.exit()
                    print("one life less")
        # aliens with stuff
        if self.aliens:
            for alien in self.aliens:
                pygame.sprite.spritecollide(alien, self.blocks, True)

                if pygame.sprite.spritecollide(alien, self.player, False):
                    pygame.quit()
                    sys.exit()

    def display_lives(self):
        for life in range(self.lives - 1):  # just to show extra lives, not current one
            x = self.lives_x_start_position + (life * (self.lives_surface.get_width() + 10))
            GAME_WINDOW.blit(self.lives_surface, (x, 6))

    def display_score(self):
        score_surface = self.font.render(f'score: {self.score}', False, 'white')
        score_rect = score_surface.get_rect(topleft=(10, 10))
        GAME_WINDOW.blit(score_surface, score_rect)

    def victory_screen(self):
        if self.aliens.sprites():
            victory_surface = self.font.render('VICTORY!', False, (255, 255, 255))
            victory_rect = victory_surface.get_rect(center=(SCREEN_WIDTH/2, SCREEN_HEIGHT/2))
            GAME_WINDOW.blit(victory_surface, victory_rect)

    def run(self):  # updating sprite groups and drawing sprite groups
        self.player.update()
        self.aliens.update(self.alien_direction)
        self.alien_position_check()
        self.player.sprite.lasers.draw(GAME_WINDOW)  # here for better control
        # self.alien_shoot_laser()  # for testing overwhelming laser shots without timer
        self.alien_lasers.update()
        self.super_alien_spawn()
        self.super_alien.update()
        self.check_collisions()

        self.player.draw(GAME_WINDOW)
        self.aliens.draw(GAME_WINDOW)
        self.super_alien.draw(GAME_WINDOW)
        self.alien_lasers.draw(GAME_WINDOW)
        self.blocks.draw(GAME_WINDOW)

        self.display_lives()
        self.display_score()
        self.victory_screen()

    player = Player


class CRT:
    def __init__(self):
        self.crt_effect = pygame.image.load('./graphics/tv.png').convert_alpha()
        # for scaling effect to screen size
        self.crt_effect = pygame.transform.scale(self.crt_effect, (SCREEN_WIDTH, SCREEN_HEIGHT))

    def crt_line_effect(self):
        line_h = 3
        line_amount = int(SCREEN_HEIGHT/line_h)
        for line in range(line_amount):
            y_position = line * line_h
            pygame.draw.line(self.crt_effect, (0, 0, 0), (0, y_position), (SCREEN_WIDTH, y_position), 1)

    def draw(self):
        # to change opacity of effect before drawing, with flicker effect with randint
        self.crt_effect.set_alpha(randint(20, 80))
        self.crt_line_effect()
        GAME_WINDOW.blit(self.crt_effect, (0, 0))


if __name__ == '__main__':  # classic init safeguard

    pygame.init()
    SCREEN_WIDTH = 600
    SCREEN_HEIGHT = 600
    GAME_WINDOW = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    pygame.display.set_caption("SPACE INVADERS")
    clock = pygame.time.Clock()
    game = Game()
    crt = CRT()  # old machine screen effect

    ALIEN_LASER_TICKER = pygame.USEREVENT+1  # ticker for fair spread of lasers
    pygame.time.set_timer(ALIEN_LASER_TICKER, 1100)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()  # double exit for certainty
            if event.type == ALIEN_LASER_TICKER:
                game.alien_shoot_laser()

        GAME_WINDOW.fill((30, 30, 30))
        # pygame.display()
        crt.draw()
        game.run()

        pygame.display.flip()
        clock.tick(60)  # fps
