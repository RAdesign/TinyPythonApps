"""Another way to create a basic Hangman game, with external wordlist"""
import random
import string
from hangman_2_words import wordlist


def validate_word(words):
    word: str = random.choice(words)
    word.replace(' ', '')
    word.replace('-', '')

    return word.upper()


def hangman():
    word = validate_word(wordlist)
    word_letters = set(word)
    alphabet = set(string.ascii_uppercase)
    used_letters = set()

    while len(word_letters) > 0:

        print('You have already used :', ' '.join(used_letters))

        word_list = [letter if letter in used_letters else '-' for letter in word]
        print('Current word:', ' '.join(word_list))

        user_letter = input("Guess a letter: \n").upper()
        if user_letter in alphabet - used_letters:
            used_letters.add(user_letter)
            if user_letter in word_letters:
                word_letters.remove(user_letter)
                print('')
            else:
                print(f'Your letter; {user_letter} is not in the word')

        elif user_letter in used_letters:
            print('Letter already used')

        else:
            print('Character not in alphabet')

    print(f'You have won, and guessed the: {word.upper()} word')


hangman()