"""simple hangman game. To be improved : Guess words from txt or other data source(like API) - randomize,
duplicate letters and words as errors, add type validation, GUI, score table. Build in main menu.
Choose difficulty level."""

number_of_attempts = 5

guess_word = "abigail"
letters_used = []
user_word = []


def find_indices(guess_word, user_guess):
    indices = []
    # use enumerate, to match index to each item in array
    for index, letter in enumerate(guess_word):
        if user_guess == letter:   # == checks both for value and index
            indices.append(index)
    return indices


def state_of_the_game():
    print("Game summary:")
    print(f"The word you`ve guessed so far: {user_word}")
    print(f"Attempts left: {number_of_attempts}")
    print(f"Used letters: {letters_used}")


# python convention to use _ for internal variables
for _ in guess_word:
    user_word.append("_")

while True:
    user_guess = input("Please guess a letter.\n")
    letters_used.append(user_guess)

    # index returns index of item matching indexed array, however, it does not include duplicates
    # print(guess_word.index(user_guess))

    indices_found = find_indices(guess_word, user_guess)
    if len(indices_found) == 0:
        print(f"The word contains no such letter as {user_guess} ")
        number_of_attempts -= 1
        state_of_the_game()
        if number_of_attempts == 0:
            print("You`ve ran out of attempts. You lose.")
            break

    else:
        for indx in indices_found:
            user_word[indx] = user_guess
        state_of_the_game()
        # print(user_word)

        if "".join(user_word) == guess_word:
            print(f" Congratulations, you have guessed the '{guess_word}' word! You win!")
            break

    # print(find_indices(guess_word, user_guess))
    # print(f"Letters already used are : {letters_used}")
state_of_the_game()

print("Thank you for playing !")
