
wordlist = ['ant', 'baboon', 'badger', 'bat', 'bear', 'beaver', 'camel', 'cat', 'clam', 'cobra', 'cougar',
            'coyote', 'crow', 'deer', 'dog', 'donkey', 'duck', 'eagle', 'ferret', 'fox', 'frog', 'goat',
            'goose', 'hawk', 'lion', 'lizard', 'llama', 'mole', 'rat', 'raven', 'rhino', 'shark', 'sheep',
            'spider', 'toad', 'turkey', 'turtle', 'wolf', 'wombat', 'zebra', 'Mickey Mouse', 'Snow White',
            'Cinderella', 'The Little Mermaid', 'Harry Potter', 'Batman', 'Spiderman', 'Captain America',
            'Winnie the Pooh', 'Curious-George', 'Charlie Brown', 'The Gingerbread Man',
            'Goldilocks and the Three Bears', 'The Three Little Pigs']