"""Expenses tracking application. To expand: type validation, input range validation,
stats sorting, months dictionary, expenses dictionary with validator, GUI, adding new expense type
 filtering one expense type."""

expenses_list = []


def add_expense(month):
    expense_amount = int(input("Please provide expense amount in $:\n"))
    expense_type = input("Please provide expense type (food, shelter, other)")

    expense = (expense_amount, expense_type,  month)
    expenses_list.append(expense)


def show_expense(month):
    for expense_amount, expense_type, month_choice in expenses_list:
        if month_choice == month:
            print(f"{expense_type} - {expense_amount}")


def show_stats(month):
    sum_up_monthly = sum(expense_amount for expense_amount, _, exp_month in expenses_list if month == exp_month)
    sum_up_total = sum(expense_amount for expense_amount, _, _, in expenses_list)
    no_of_exp_this_month = sum(1 for _, _, exp_month in expenses_list if exp_month == month)
    avg_exp_this_month = sum_up_monthly/no_of_exp_this_month
    no_of_exp_total = sum(1 for _, _, exp_month in expenses_list if exp_month == month)
    avg_exp_all = sum_up_total / no_of_exp_total

    print("Stats:")
    print(f"All expenses for {month} month, is {sum_up_monthly} $")
    print(f"All expenses amount up to {sum_up_total} $")
    print(f"Average expense for {month} month, is {avg_exp_this_month} $")
    print(f"Average expense for all months, is {avg_exp_all} $")


print("Welcome to the Expense Tracker DeLuxe Limited.")
while True:

    chosen_month = int(input("Please, choose a month you want to check. (1-12)"))
    if chosen_month == 0:
        break

    while True:
        print("Menu:")
        print("0 : Choose another month.")
        print("1 : Show all expenses.")
        print("2 : Add an expense.")
        print("3 : Stats.")
        menu_choice = int(input("Please, choose an option: \n"))

        if menu_choice == 0:
            break  # exits this while loop
        if menu_choice == 1:
            show_expense(chosen_month)
        if menu_choice == 2:
            add_expense(chosen_month)
        if menu_choice == 3:
            show_stats(chosen_month)
