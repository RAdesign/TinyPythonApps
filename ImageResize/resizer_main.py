"""an image resizer using Pillow library"""
from PIL import Image

image_to_resize = Image.open('image_to_resize.jpg')

print(f"Current image size in px is: {image_to_resize.size}")

resized_image = image_to_resize.resize((200, 200))
resized_image.save('resized_image.jpg')

print(f"Current image size in px is: {resized_image.size}")