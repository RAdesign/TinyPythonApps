from matplotlib import pyplot as matplot

x = [2, 3, 5, 7]
y = [2, 4, 6, 8]

x1 = [1, 2, 3, 5]
y2 = [5, 4, 2, 0]

left_col = [2, 4, 6, 8]
right_col = [12, 14, 16, 18]
columns_label = ['adin', 'dwa', 'Drei', 'four']
matplot.bar(left_col, right_col, tick_label=columns_label, width=0.8, color=['grey', 'orange'])

"""matplot.plot(x, y, color='red', linestyle='dashed', linewidth=2, marker='o',
             markerfacecolor='green', markersize=12, label='Main Line')

matplot.ylim(1, 8)
matplot.xlim(1, 8)
matplot.plot(x1, y2,  label='Second Line')"""

matplot.xlabel('X Axis')
matplot.ylabel('Y Axis')
matplot.title('Demo')
matplot.legend()  # print legend with labels

matplot.show()