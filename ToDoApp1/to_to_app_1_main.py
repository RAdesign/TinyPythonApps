"""Simple, basic ToDo App with write read to file."""

user_option = 0

user_tasks = []


def show_tasks():
    task_index = 0
    for task in user_tasks:
        print(f"{task} [{str(task_index)}]")
        task_index += 1


def add_task():
    task = input("Please write down your task:\n")
    if task == "0":
        print("Return to menu")
    else:
        user_tasks.append(task)
        print(f"Task [{task}] has been added.")


def delete_task():
    task_index = int(input("Please provide an index of a task to delete.\n"))

    if task_index < 0 or task_index > len(user_tasks)-1:
        print("There is no task with such index.")
        return

    user_tasks.pop(task_index)
    print("Task has been deleted.")


def save_tasks_to_file():
    with open("tasks.txt", "w") as file:
        for task in user_tasks:
            file.write(task+"\n")


def load_tasks_from_file():
    with open("tasks.txt", "r") as file:
        for line in file.readlines():
            user_tasks.append(line.strip())


# load_tasks_from_file()

while user_option != 5:
    if user_option == 1:
        show_tasks()
    if user_option == 2:
        add_task()
    if user_option == 3:
        delete_task()
    if user_option == 4:
        save_tasks_to_file()

    print("MENU")
    print("1.Show tasks")
    print("2.Add task")
    print("3.Delete task")
    print("4.Save tasks to file")
    print("5. Exit")
    user_option = int(input("Choose your option: \n"))
