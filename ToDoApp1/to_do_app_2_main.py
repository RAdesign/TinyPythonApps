"""Simple, basic ToDo App with write read to SQLite database."""

import sqlite3


sqlit_connection = sqlite3.connect("ToDo2.db")


def create_table(connection):
    try:
        cursor = connection.cursor()
        cursor.execute("""CREATE TABLE task(task text)""")
    except:  # too broad intentionally left out
        pass


def show_tasks(connection):
    cursor = connection.cursor()
    cursor.execute("""SELECT rowid, task FROM task""")
    result = cursor.fetchall()
    for row in result:
        print(f"Task no.: {row[0]} | Task: {row[1]}")


def add_task(connection):
    task = input("Please write down your task:\n")
    if task == "0":
        print("Return to menu")
    else:
        cursor = connection.cursor()
        cursor.execute("""INSERT INTO task(task) VALUES(?)""", (task,))  # tuple as in documentation
        connection.commit()
        print(f"Task [{task}] has been added.")


def delete_task(connection):
    task_index = int(input("Please provide an index of a task to delete.\n"))
    cursor = connection.cursor()
    rows_del = cursor.execute("""DELETE FROM task WHERE rowid=?""", (task_index,)).rowcount
    if rows_del == 0:
        print("Task does not exists.")
    else:
        print("Task removed")
    connection.commit()

    print("Task has been deleted.")


create_table(sqlit_connection)

while True:
    print("MENU")
    print("1.Show tasks")
    print("2.Add task")
    print("3.Delete task")
    print("4.Exit")
    user_option = int(input("Choose your option: \n"))
    if user_option == 1:
        show_tasks(sqlit_connection)
    if user_option == 2:
        add_task(sqlit_connection)
    if user_option == 3:
        delete_task(sqlit_connection)
    if user_option == 4:
        break

sqlit_connection.close()
